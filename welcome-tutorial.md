# "Welcome Tutorial" dialog

The "Welcome Tutorial" dialog appears the first time a user runs GNOME Boxes in their system. The underlying setting that triggers this can be set with:

`gsettings set org.gnome.boxes first-run true`

## Adding new pages to the Tutorial

The [data/ui/welcome-tutorial.ui](https://gitlab.gnome.org/GNOME/gnome-boxes/tree/master/data/ui/welcome-tutorial.ui) file holds the content definitions for the dialog.

Each `GtkStack` child is a page. The page order is respected.

A page is an object of type [Boxes.WelcomeTutorialPage](https://gitlab.gnome.org/GNOME/gnome-boxes/tree/master/data/ui/welcome-tutorial-page.ui). It has the following properties:
* **title**: a string
* **description**: a string
* **color**: a RGB code for the background color
* **image**: the resource path of the image file illustrating the page