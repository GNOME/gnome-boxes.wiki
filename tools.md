GNOME Boxes is powered by various free and open source software libraries such as:

- [libosinfo](https://gitlab.gnome.org/GNOME/gnome-boxes/-/wikis/tools/libosinfo): provides tools to inspect operating system bootable images and match these to a database of recommended configurations for the respective operating system.
- libvirt: manages the creation/deletion/cloning of virtual machines as well as the assignment of virtual devices to it.
- qemu: is the emulator/virtualizer.