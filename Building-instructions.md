The recommended method to build and develop GNOME Boxes is with [Flatpak](http://docs.flatpak.org/). This way we can guarantee reproducible builds across different Linux distributions and environments. At the end of this page, we describe shortly how to build GNOME Boxes against your host system (the traditional way).

## Building in a Flatpak container

The org.gnome.BoxesDevel.json file is our Flatpak manifest which, among other things, describes the list of required modules and their respective versions and build commands.

### GNOME Builder

[GNOME Builder](https://wiki.gnome.org/Apps/Builder) is able to detect the presence of a Flatpak manifest in the project's source tree and orchestrate the build within a click of buttons. This process is described step by step in the [GNOME Newcomers tutorial](https://wiki.gnome.org/Newcomers/).

The first build should take a while since this process will download all the modules listed in the manifest and build them one after the other. Builder will also attempt to download the Flatpak runtime and SDK specified in the manifest if it is not present in your current installation.

After the build is finished, you should be able to run Boxes by pressing the Play button in the top bar in GNOME Builder. You can perform changes in the source code and press the Play button again to see your work in action.

GNOME Builder will also smartly cache the modules build, avoiding rebuilding everything when it is unnecessary.

### Flatpak Builder

flatpak-builder is a command-line utility usually available alongside Flatpak that enables you to build and install Flatpaks from the project's source code.

The basic usage is to invoke:

```
$ flatpak-builder <build-dir> build-aux/flatpak/org.gnome.BoxesDevel.json
```

Where `<build-dir>` is the path to the directory that Boxes will be built into.

Runtimes and SDKs are usually already present in your system if you have installed Flatpak apps or have attempted to develop another application with Flatpak, but if you need to manually install them, you can run:

```
$ flatpak install gnome-nightly org.gnome.Platform
$ flatpak install gnome-nightly org.gnome.Sdk
```

Where [gnome-nightly](https://wiki.gnome.org/Apps/Nightly) is the repository where the unstable (master) org.gnome.Platform runtime and the org.gnome.Sdk SDK are hosted.

Visit https://wiki.gnome.org/Apps/Nightly to learn how to add the `gnome-nightly` repository.

## Building against your host system

Distribution package managers often offer tools to install the build dependencies of existing packages. You can benefit from that because Boxes is available in the most popular Linux distro repositories, such as Fedora, Ubuntu, Debian, Arch, etc...

In Fedora/RHEL/CentOS and other RPM based systems you should be able to invoke:

```
$ sudo dnf builddep gnome-boxes
```

In Ubuntu/Debian and other .deb based systems you should be able to invoke:

```
$ sudo apt build-dep gnome-boxes
```

After installing the dependencies, you should open a terminal in the Boxes source tree and run:

```
meson _build
```

Where `_build` is the path to the directory that Boxes will be built into. At this moment the meson build system might complain about missing dependencies. You need to use your distribution's package manager to install these missing dependencies. It might happen that your distribution doesn't provide a version of a dependency which is as new as the one which Boxes requires. In this case, you will need to download the dependencies source code, build, and install it manually (this can create conflicts with other applications/libraries installed in your system). It is highly recommended to use a container instead.

If all dependencies are present in your system, you can now finally build.

```
$ cd _build
$ ninja
```

The compiler and other tools will be invoked and error messages will be printed in your terminal. If the compilation succeeds you will find the executable in **_build/src/gnome-boxes**. Once you edit the source code to introduce new changes, you should call "ninja" in the build directory to build and test your changes.