See https://libosinfo.org

Libosinfo provides tools to inspect operating system bootable images and match these to a database of recommended configurations for the respective operating system.

Libosinfo is actually made of three parts:

## osinfo-db

Is a database of operating systems in the XML format with entities such as: Os (operating system) and device.

A typical `Os` entity provides information about operating systems that is used by Boxes such as:
* `<name>`, `<version>`, `<vendor>`, provide user-facing name, version and vendor
* `<release date>`
* variants (OS variants such as desktop, server, netinstall, cloud...)
* medias: OS images with detection information (unique metadata in the media that can be used to identify the OS in the media).
  * `<url>`: publicly available address for downloading the OS image
  * `<iso>`: has subnodes matching metadata properties used for identifying the OS in the media. These subnodes are usually `<volume-id>` and `<system-id>`. A regular expression (regex) can be used.
* The `<resources>` node describes the `<minimum>` and `<recommended>` resources required for running the operating system. Boxes accesses `<n-cpus>` (number of CPUs), `<cpu>`, `<ram>`, `<storage>`.

In RPM based systems the osinfo-db is usually installed in `${_datadir}/osinfo/`. In the Boxes Flatpak, the osinfo-db is installed at `/app/share/osinfo-db/`.

See also [The Libosinfo Database Layout](https://gitlab.com/libosinfo/osinfo-db-tools/-/blob/master/docs/database-layout.txt)

## osinfo-db-tools

Provides utility programs to manage the Osinfo database.

## libosinfo

Provides programmatic access to osinfo database. It uses GLib and GObject introspection, which allows Boxes to consume it in the Vala programming language. 

libosinfo also provides utilities that are useful in the development and debugging of Boxes.

* osinfo-detect: receives an OS image (usually a .iso file) and matches its metadata to the regex expressed in the `<media>` entity node , to identify the operating system contained in the OS image file.
* osinfo-query: allows for performing search queries in the osinfo database. Some useful queries for developing Boxes are:
  * `$ osinfo-query os`: lists all OSes in the database.