# GNOME Boxes: Developer guide

Welcome to the **GNOME Boxes Developer guide**! This wiki is focused on developers, designers, and translators, contributing or interested in understanding the project internals. If you are looking for usage tips and instructions, you can find it at the [User Documentation](https://help.gnome.org/users/gnome-boxes/stable/index.html) (translated in various languages).

## Documentation topics

This guide intends to provide information to help new contributors and also serve as a reference for experienced contributors.

### [Building instructions](Building-instructions )

How to build GNOME Boxes in a Flatpak container or in your host operating system.

### [Coding style guide](style-guide)

Useful tips for formatting your code, naming things, and structuring your code.

### [Architecture overview](source-overview)

Learn about the various folders and files in the Boxes source tree, and how things are structured.

### [Debugging](debugging-crashes)

Find useful tips for debugging Boxes.

### [Releasing](Releasing)

A description of the process of writing release notes and publishing a Boxes release.

### [Tooling](tools)

Useful links to understand the functionality of various tools used in the project's development process.