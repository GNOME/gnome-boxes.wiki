We support two building methods, classic and Flatpak.

# Debugging a local install (classic)

Install the [debug symbol packages for your distribution](https://wiki.gnome.org/GettingInTouch/Bugzilla/GettingTraces).

 * Run Boxes with gdb with `gdb gnome-boxes`
 * On the gdb console, enter `run`.

# Flatpak

 * Install the debug symbols

```
flatpak install flathub org.gnome.Boxes.Debug
flatpak install flathub org.gnome.Platform.Debug
```
 * Open a shell session inside the Flatpak container with `flatpak --devel --command=bash run org.gnome.Boxes`
 * Run Boxes with gdb with `gdb /app/bin/gnome-boxes`

# Reproduce the crash

Now Boxes should be opened, and you should proceed to perform the same action that caused your crash.

Once Boxes crashes, get back to the terminal with the gdb console and type:

`bt all`

## Sharing your backtrace

Copy everything printed on your terminal since you opened gdb, and paste in a text file. Attach your file to the GitLab issue where your crash is being reported.

# Activating debug messages

To run Boxes with debug message output on the console, just run:

```bash
G_MESSAGES_DEBUG=Boxes gnome-boxes
```

In Flatpak, run:

```bash
flatpak run --env=G_MESSAGES_DEBUG=Boxes org.gnome.Boxes
```

In jhbuild, run:

```bash
G_MESSAGES_DEBUG=Boxes jhbuild run GNOME-boxes
```