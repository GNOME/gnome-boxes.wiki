# DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT

# Source Code Structure

Our code base is very similar to most of GNOME projects. Although there isn't a rule per-see, most of GNOME projects tend to separate their sources as the following:

* **build-aux/** contains utilities that are used during the compilation and installation process.
  * [post_install.py](https://gitlab.gnome.org/GNOME/gnome-boxes/blob/master/build-aux/post_install.py): performs post install tasks such as: updating the system's desktop file database, update the system's icon cache, and compile and install the project GSetting XML schemas.
  * **flatpak/** contains the artifacts used for building a Flatpak such as the manifest, but also patches that we ship for modules included in the Boxes Flatpak.
    * [org.gnome.Boxes.json](https://gitlab.gnome.org/GNOME/gnome-boxes/blob/master/build-aux/flatpak/org.gnome.Boxes.json): is the Flatpak manifest that orchestrates the build and lists everything that should be contained in a Boxes Flatpak.
* **data/** contains the non-code resources that Boxes needs, such as icons, appdata, desktop file, gschemas, and UI files.
  * **osinfo/** the osinfo/ directory contains overwrites that Boxes performs on top of [osinfo-db](https://gitlab.com/libosinfo/osinfo-db). Nowadays we mostly overwrite the \<logo\> property, linking to [our logos collection](gitlab.gnome.org/gnome/gnome-boxes-logos).
  * **ui/** contains the user interface files. See the [GNOME documentation for "Building user interfaces"](https://developer.gnome.org/gtk3/stable/ch01s03.html). Although these files can be edited in Glade, we usually write them manually.
  * gnome-boxes-search-provider.ini: specifies our SearchProvider API. This is the mechanism that allows Boxes VMs to show up in the GNOME Shell search results. See the [GNOME documentation for "Search Providers"](https://developer.gnome.org/SearchProvider/).
  * gtk-style.css: contains our overwrites in the toolkit theme and Boxes-specific CSS style classes.
  * org.gnome.Boxes.appdata.xml.in: is our appdata file which specifies how Boxes is presented in Software centers such as GNOME Software and Flathub. Check the [AppStream "Metadata" documentation](https://www.freedesktop.org/software/appstream/docs/chap-Quickstart.html) for more details.
  * org.gnome.Boxes.desktop.in: See [Desktop Entry Specification](https://standards.freedesktop.org/desktop-entry-spec/latest/index.html#introduction).
  * org.gnome.boxes.gschema.xml: These is the Boxes GSettings XML schema. We store in gsettings some convinient settings for the application usage such as the last used window size and view type (list view or icon view). Our current implementation of Shared Folders uses GSettings to map the host folders to the VM's.
  * recommended-downloads.xml: Contains a hardcoded list of Osinfo OSes that are promoted in the "Download an OS" page. This allows for distributions to pick their own recommended-downloads.
* **help/** contains the source for the user documentation available in the *Help* menu option, in the gnome-help app, and online at https://help.gnome.org/users/gnome-boxes/stable/index.html
* **po/** contains the translations.
* **src/** contains the source code.
* **subprojects/** contains the source code for libraries bundled within Boxes. These projects are developed separately in their own git repositories.
  * [libovf-glib](https://gitlab.gnome.org/felipeborges/libovf-glib): implements the support for `.OVA` and `.OVF` files.
* **tests/** contains the automated tests.
* **vapi/** contains Boxes-specific overrides to the Vala language APIs.
