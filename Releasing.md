Boxes follows the [GNOME release schedule](https://wiki.gnome.org/Schedule).

We usually branch out the development branch at the first freeze of the release schedule (API/ABI, Feature, Feature, UI). This allows us to continue landing changes in the development branch while stabilizing the release branch.

# Releasing

The r[elease process is the one described in the GNOME Wiki](https://wiki.gnome.org/MaintainersCorner/Releasing). The Boxes particularities are described bellow:

## 1. Writing release notes

### NEWS file

The [NEWS](https://gitlab.gnome.org/GNOME/gnome-boxes/-/blob/master/NEWS) file describes the changes in a public facing format. Internal implementation details should be avoided in favor of descriptive topics of final user's interest.

The content of this file appears in the email announcing the release that is automatically sent to `ftp-release-list@gnome.org`

We have a Python script at [build-aux/release-notes.py](https://gitlab.gnome.org/GNOME/gnome-boxes/-/blob/master/build-aux/release-notes.py) which outputs the commit messages since the last release, separating translation commits into a different section. The output of this script is not meant to be committed as it is. This script was written with the goal of speeding up the process of writing release-notes. A human should still craft the final text before including it in the release.

The Boxes NEWS file...

* Has a header section with the release number and date
* Lists the changes in a user-facing text format
* Lists the changes in translations by listing their language names
* Lists all contributors to the release in alphabetical order

### Appstream file

The Appstream file, also known as appdata file, is used by software stores/catalogs such as GNOME Software and KDE Discovery to present a description of the application.

The `<releases>` section describes the release changes.

## Publishing the release

The workflow for publishing releases is:

1. Create a topic branch for the merge-request
2. Bump the version in the meson.build file
3. Add the NEWS, appstream, and meson.build changes to git stage
4. Make a signed release commit
5. Push the topic branch to GitLab so it can run the CI to validate the build
6. Merge it to the release branch
7. Pull the changes and tag the release commit
8. Push the release tag
9. Produce a local tarball
10. Upload the local tarball to ftp.gnome.org

See the sections below for some details of these steps.

### meson.build file

The meson.build file is the main configuration file for the Meson build.

During the release process, the version parameter in the meson.build is bumped.

### Commit

The release commit includes the changes in the NEWS, Appstream, and meson.build files.

In Boxes, the release commit is traditionally [signed](https://git-scm.com/book/en/v2/Git-Tools-Signing-Your-Work).

The commit message is a one-liner in the format `Release <release-number>`.

This is an example of a release commit: [fd24d6e2](https://gitlab.gnome.org/GNOME/gnome-boxes/-/commit/fd24d6e2).

### Tag

Distributions and other types of downstreams can often use git tags instead of tarballs for obtaining a newly released version of Boxes. For this reason, GNOME apps tag their release commits.

In Boxes the git tag name is simply the release version.

This is an example of a release tag: [41.alpha](https://gitlab.gnome.org/GNOME/gnome-boxes/-/tags/41.alpha)
