# 3D Acceleration

The Boxes 3D acceleration feature for guest is powered by [virlgrenderer](https://virgil3d.github.io/).

We have gotten a few bug reports after auto-enabling 3D acceleration for all guests that libosinfo reports to support virtio-gpu.

https://github.com/flathub/org.gnome.Boxes/issues/22
https://gitlab.gnome.org/GNOME/gnome-boxes/issues/324
https://gitlab.gnome.org/GNOME/gnome-boxes/issues/341

The way it is implemented NOW is that we try to enable 3D acceleration by default for all guests that support virtio-gpu and we offer a toggle option in the VM properties to disable that in case of issues. This setting is saved in the VM's keyfile, under the **3daccel** property.

The UX logic is:
* If the "3daccel" flag is not set, we throw a notification offering the user to disable 3D acceleration in case they are having issues. Once the user interacts with the notification, the keyfile is written:
  - if the notification is dismissed, 3daccel = true
  - if the user clicks "Disable", 3daccel = false