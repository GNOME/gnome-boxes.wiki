# OS Downloads (recommended downloads) page

The box creation Assistant has a "**Download an OS**" page which lists some freely available operating systems that Boxes can download. The list data is powered by [libosinfo](https://gitlab.gnome.org/GNOME/gnome-boxes/-/wikis/tools/libosinfo).

Distributors are welcome to customize their recommended-downloads list. The default recommended-downloads list file is hosted at https://gnome.pages.gitlab.gnome.org/gnome-boxes-logos/recommended-downloads.xml

Distros can point to their very own recommended-downloads.xml file by overriding the `org.gnome.boxes recommended-downloads-url` [GSetting](https://gitlab.gnome.org/GNOME/gnome-boxes/-/blob/main/data/org.gnome.boxes.gschema.xml).

> Setting `org.gnome.boxes recommended-downloads-url` to an empty string will make Boxes use its offline recommended-downloads.xml file ([distributed within the source code](https://gitlab.gnome.org/GNOME/gnome-boxes/-/blob/main/data/recommended-downloads.xml)) instead of the remote address.

## Overriding the recommended-downloads list

Currently one can override the following properties of an OS entry in the list:
* `<title>`: the label identifying the operating system
* `<subtitle>`: a description and/or name of the vendor providing the operating system
* `<url>`: an URL pointing to an ISO/image file to install the operating system

### Example

```xml
<os id="http://gnome.org/gnome/nightly">
  <title>Custom GNOME OS Nightly Title</title>
  <subtitle>The GNOME Project</subtitle>
 <url>https://os.gnome.org/download/latest/gnome_os_installer.iso</url>
</os>
```

### Testing the changes locally

You can run a simple Python http server hosting your recommended-downloads.xml file with

`python -m  http.server 8000`

And use `gsetting set org.gnome.boxes recommended-downloads-url <url-to-recommended-downloads.xml>` to set the new address from where Boxes will load its recommended downloads. 

## Proposing new entries to the recommended-downloads.xml list

We encourage distributors to host their own recommended-downloads.xml file instead.

If an OS entry makes sense to the overall GNOME Boxes userbase, it can be suggested by filing a Gitlab issue at https://gitlab.gnome.org/GNOME/gnome-boxes-logos/

The new added operating system needs to have a matching entry in [osinfo-db](https://gitlab.com/libosinfo/osinfo-db).